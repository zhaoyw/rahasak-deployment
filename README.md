# Rahasak Blockchain

Rahasak is a permissioned Blockchain system which is targeted for scalable, 
enterprise-level applications such as big data, smart city, IoT. Rahasak addressed 
three main performance bottlenecks on existing Blockchain platforms, namely, 
`Order-Execute architecture`, `Full node data replication` and `Imperative style 
smart contracts`. Rahasak blockchain uses `Apache Kafka`-based consensus and 
proposes novel `Validate-Execute-group` blockchain architecture to handle real-time 
transaction execution on the blockchain. This architecture is equipped with a 
`Functional Programming` and `Actor`-based `Aplos `smart contract platform that 
enables concurrent execution of transactions in the blockchain(`Aplos` smart
contract platform introduced as `Smart Actor` platform since it built on top of 
Actor based concurrency handling). `Rahasak-ML` federated machine learning service 
in Rahasak blockchain allows one to build and share ML models with privacy preserving 
manner. Rahasak supports high transaction throughput, high scalability, concurrent 
transaction execution, sharding-based data replication, data analytics and machine 
learning features.

### Rahasak Architecture

Most current blockchain systems are built as monolithic systems. A single program
/service on the blockchain handles all the features in the blockchain. This includes 
handling consensus, maintaining the decentralized ledger, broadcasting transactions, 
checking double spends, etc. We believe that this is not an ideal design for a 
distributed system environment. In a monolithic system approach, one needs to build 
everything using a single programming language. When the code base grows, it becomes 
unwieldy. Since only one service is available, it is not possible to scale. As such, 
we build Rahasak using a `Microservice` architecture, solving all the aforementioned 
problems. In Rahasak, all the functionalities are implemented as small services 
(Microservices). All these services are `Dockerized` and available for deployment 
using `Kubernetes(k8s)`. Following figure shows the architecture of Rahasak. Single 
node of Rahasak blockchain contains four main services. `Apache Kafka` has been used 
for the consensus and the message broker.

![Alt text](figures/rahasak-v1.png?raw=true "rahasak microservices architecture")

```
1. Aplos service - Smart contract service implemented using Scala functional programming language and 
   Akka actors. It supports concurrent transactoin execution on blockchain
2. Storage service - Apache Cassandra based block, transaction and asset storage. It supports sharding 
   based data replication
3. Lokka service - Block generating service implemented using Scala and Akka Streams
5. Rahasak-ML - Federated machine learning service. It allows to build and share ML models with privacy 
   preserving manner.
5. Kafka message broker - Apache Kafka based distributed publisher/subscriber service. it used for consens 
   and the message broker
```

### Aplos Service

The Aplos service is the actor-based smart contract service in Rahasak. All 
blockchain-based software programs and the messages that pass between them are 
written as Akka actors and saved in the Aplos service. Clients send transaction 
requests to this service with the actor name and its message. Based on that, the 
Aplos service finds the corresponding actor and passes the message to that actor. 
Then the actor validates and executes the transaction message. Based on the 
validate/execute outcome, it creates a transaction and updates the asset state 
in the underlying asset storage, as shown in the following Figure. With `Validate-
Execute-Group` architecture, transactions will be executed only in one peer; With 
Aplos smart contracts, the transactions can be executed concurrently. Since
Aplos smart contrats built on top of Actor based consurrency handling it
introduced as a `Smart Actor` platfrom.


![Alt text](figures/rahasak-aplos-architecture.png?raw=true "aplos service architecture")

Each node in the network runs its own Aplos service. This service consumes 
transaction messages from Apache Kafka. There is a Kafka topic which the service 
listens to. A client publishes transaction messages to this Kafka topic. These Kafka 
topics can be partitioned and operate as a Kafka consumer group. Then Kafka handles 
the message partitioning and message broadcasting between the topic, guaranteeing 
total order(provide total order by sending a message only to one consumer by topic 
partitioning). With partitioned topic Rahasak can run the multiple Aplos services 
in a single blockchain peer depends on the transaction load. Each Aplos service 
consumes transaction only once and executes them. As shown in following figure, 
they can work independently and execute transactions concurrently.

![Alt text](figures/rahasak-kafka-topic.png?raw=true "aplos kafka topic partition")

### Storage Service

Storage is the place where the blocks, transactions and assets are stored in 
Rahasak.  We have used eventually consistent distributed database as the asset 
storage in Rahasak. Each peer in Rahasak blockchain comes with two types of storage, 
off-chain storage and on-chain storage. Off-chain storage stores the actual data 
generated by the peers. The hash of these data published to on-chain storage and 
shared with other peers. As shown in following figure , the on-chain nodes are 
connected in a ring cluster. Once Aplos service executes transactions, it will 
write the state updates and transactions into its storage service instance(distributed 
database node). Then the saved data will be distributed to other nodes via the 
underlying distributed database.

![Alt text](figures/rahasak-storage-architecture.png?raw=true "storage service architecture")

Rahasak blockchain does not use full node data replication like Bitcoin or other
 blockchain platforms. Instead, Rahasak adopts a sharding-based data replication 
mechanism. In Rahasak blockchain, data replications are handled by the underlying 
distributed database. When one blockchain node writes data to its storage service, 
the data will be replicated to only a certain number of blockchain nodes depending 
on the replication factor defined in the data replication procedure of the distributed 
database.

We have chosen Apache Cassandra as our storage service as we can write to any node 
in the Cassandra cluster. In other database systems, one can only write to the 
master node. There is no master node on Cassandra, which has a masterless ring 
architecture. In the blockchain, every node should have equal write ability to 
the data storage. We also use Cassandra for its scalability and high-write throughput. 
Cassandra supports up to one million writes per second, which is an ideal data 
storage for a high-transaction throughput environment (e.g., banking applications). 
All the data in `Transactions`, `Blocks` and `Asset` tables will be indexed in 
`Apache Lucene` index-based API to achieve full-text search capability of the 
Rahasak blockchain.

### Lokka Service

Lokka service creates blocks in the `Group` Phase of `Valida-Execute-Group` 
blockchain architecture. When creating a block, it generates the block hash and 
adds the transaction list to the block as a Cassandra user-defined type list. 
Block hash contains the previous block hash and Merkle root hash of the transaction 
list. Finally, the block is saved in the Blocks table. The newly created block ID 
(primary key of the block in Blocks table) is broadcast to other Lokka services 
via Kafka (each Lokka service has their own Kafka topic for communication). Then 
other Lokka services take the block from the Blocks table, verify the transactions 
in the block and digitally sign the block. When signing, they digitally sign the 
block hash and add the signature into the block header. The block generation and 
signing process happen in a fully asynchronous way.

There are multiple Lokka services in the system (each blockchain node may run their 
own Lokka service). Lokka services create blocks based on a time interval (e.g., 
create a block every second) or based on the number of transactions (e.g., after 
every 100 transactions). The Block Creator is determined in a round-robin distributed 
scheduler. Consider the scenario in following Figure, which has 3 Lokka services. 
Assume that the first block is created by Lokka A, the second block will be created 
by Lokka B and the third block is by Lokka C. This goes on repeatedly.

![Alt text](figures/rahasak-lokka-architecture.png?raw=true "lokka service architecture")

### Apche Kafka

Apache Kafka used as the consensus platform and message broker in Rahasak blockchain. 
All transactions published by the clients will be stored and ordered in a Kafka 
message broker. Aplos services take the ordered transactions from a Kafka message 
broker, execute them with smart contracts. Kafka message brokers in Rahasak focus 
on two main scenarios. In the first scenario, the client publishes transaction 
messages to the gateway service via Kafka message broker. There is a separate 
Kafka message topic for each Aplos service in the blockchain peers. Clients publish 
transaction messages to these Kafka topics. By using Kafka for client-to-gateway 
service communication, we can handle back-pressure operations with handling a high 
transaction load in the scalable application. The second scenario is communication 
between Lokka services. When a Lokka service generates a block and saves it in the 
Blocks table, the block ID is broadcast to other Lokka services via Kafka for approval. 
All communication between Lokka services happens through Kafka. We run 3 Kafka 
broker nodes with 3 Zookeeper nodes in Rahasak.

### Rahasak-ML 

Rahasak-ML service is the data analytics and machine learning platform of Rahasak 
blockchain. It integrates federated learning with blockchain to ML model sharing 
and continuous model training without having a centralised coordinator. Each peer 
in Rahasak blockchain has off-chain storage and on-chain storage. Blockchain peers 
can establish supervised or unsupervised machine learning models with the existing 
data on its off-chain storage. Once a peer generates a model it can be incrementally 
trained and aggregated by other peers through the blockchain by using the federated 
learning approach. The model parameter sharing, local model generation, incremental 
model training, model sharing functions implemented in Rahasak-ML platform. Once ML 
models are generated these models can be integrated into Rahasak blockchain smart 
contracts to do the predictions of real-time data. 

### Rahasak-CA

Rahasak-CA is the certificate authority in Rahasak blockchain. The blockchain peers 
and clients public key certificates are issued by the Rahasak-CA. Apache Cassandra 
storage has been used as the certificate storage in Rahasak-CA. All the certificates 
issued by the Rahasak-CA will be stored in the Apache Cassandra storage. When 
bootstrapping a blockchain peer, it will generate a public-private key pair and 
submits the public key into Rahasak-CA. Then Rahasak-CA will digitally sign that 
public key and store it in the certificate storage. Then these certificates will be 
available for other peers and client in the blockchain network. Rahasak-CA exposes 
REST based API and Apache Kafka based streaming API to communicate. There are two 
main APIs available, 1) certificate publish API, 2) certificate search API. Blockchain 
peers and clients submit their public keys into the certificate store via certificate 
publish API. The certificates in the Rahasak-CA can be searched via the certificate 
search API.

# Service Deployment

Rahasak blockchain can be deployed with `Docker` and `Kubernetes`. This repository 
contains single node [docker-deployment](https://gitlab.com/rahasak-core/rahasak-deployment/-/tree/master/docker-deployment) 
and [k8s-deployments](https://gitlab.com/rahasak-core/rahasak-deployment/-/tree/master/k8s-deployment) 
of Rahasak blockchain. Please get the respective deployments from the given repositories
and continue the deployment.
