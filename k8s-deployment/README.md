# Kubernetes Deployment

This repository contains the single node `Kubernetes` deployment of Rahasak 
blockchain with aplos smart actor platform. Aplos smart actor service deployed 
with a account based money transferring smart actor.  There is separate
`docker-compose` deployment of the Rahasak blockchain in the
[docker-deployment](https://gitlab.com/rahasak-core/rahasak-deployment/-/tree/master/docker-deployment) 
repository.

### deploy elassandra

```
# create elassandra pod and service
kubectl create -f elassandra-pod.yaml
kubectl create -f elassandra-service.yaml

# you can access elassandra from host ip now
cqlsh 10.4.1.70 9042 --cqlversion 3.4.4
curl 10.4.1.70:9200
```


### deploy promize-lokka

```
# create promize-lokka pod
kubectl create -f promize-lokka-pod.yaml
```


### deploy promize-api

```
# create promize-api pod and service
kubectl create -f promize-api-pod.yaml
kubectl create -f promize-api-service.yaml

# you can access promize api from your host
curl -XPOST "http://10.4.1.70:7070/promize" -d '{"Uid":"69B12057-40D0-41B1-9965-52FE1FC538DE","Type": "INIT", "FromAddress":"2222", "ToAddress": "2222", "Amount": 1000}'
```
